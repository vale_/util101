//! Rust abstractions for KingsIsle's ObjectProperty reflection system.

#![deny(unsafe_op_in_unsafe_fn, rustdoc::broken_intra_doc_links)]
#![feature(core_ffi_c)]

#[macro_use]
extern crate static_assertions;

mod property_class;
pub use self::property_class::*;

mod property_list;
pub use self::property_list::*;

mod property;
pub use self::property::*;

mod typ;
pub use self::typ::*;
