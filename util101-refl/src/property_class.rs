use std::ffi::c_void;

use crate::PropertyList;

// PropertyList *PropertyClass::GetPropertyList();
type Vtable18 =
    unsafe extern "fastcall" fn(/* this: */ *mut PropertyClass) -> *mut PropertyList;

/// Representation of the PropertyClass type, the heart of the ObjectProperty
/// reflection system.
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct PropertyClass {
    vtable: *mut c_void,
}

impl PropertyClass {
    /// Gets the [`PropertyList`] of a class, given a pointer to the object.
    ///
    /// # Safety
    ///
    /// The pointer must be non-null, well-aligned and point to a valid C++
    /// `PropertyClass` allocation.
    pub unsafe fn property_list(this: *mut PropertyClass) -> *mut PropertyList {
        unsafe {
            let getter: Vtable18 = cxx::get_vtable_function(this as *const c_void, 3);
            (getter)(this)
        }
    }
}
