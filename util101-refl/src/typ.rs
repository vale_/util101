use std::ffi::{c_uint, c_void};

use crate::PropertyList;

/// A generic, reflected data type.
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct Type {
    vtable: *mut c_void,
    _08: [u8; 0x30],
    pub name: cxx::Str,
    pub type_hash: c_uint,
    _5c: [u8; 4],
    pub size: c_uint,
    _64: [u8; 36],
    pub is_pointer: bool,
    pub is_ref: bool,
    pub property_list: *mut PropertyList,
}

assert_eq_size!(Type, [u8; 0x98]);
