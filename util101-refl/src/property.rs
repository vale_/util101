use std::ffi::{c_char, c_uint, c_void};

use crate::{PropertyList, Type};

/// A property describing a reflected `PropertyClass` member inside the
/// [`PropertyList`].
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct Property {
    vtable: *mut c_void,
    _08: [u8; 0x30],
    pub list: *mut PropertyList,
    pub container: *mut c_void,
    _48: [u8; 0x8],
    id: c_uint,
    _54: [u8; 0x4],
    pub name: *mut c_char,
    pub name_hash: c_uint,
    pub full_hash: c_uint,
    pub offset: c_uint,
    _6c: [u8; 0x4],
    pub typ: *mut Type,
    _78: [u8; 0x8],
    pub flags: c_uint,
    _84: [u8; 0x4],
    pub note: *mut c_char,
    pub psinfo: *mut c_char,
    pub options: cxx::Vector<Option>,
}

assert_eq_size!(Property, [u8; 0xB0]);

/// A type that stores information about a property's enum options.
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct Option {
    pub value: cxx::Str,
    pub int_value: c_uint,
    pub name: cxx::Str,
}

assert_eq_size!(Option, [u8; 0x48]);

/// Representation of a reflected member function in a `PropertyClass`.
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct Function {} // TODO
