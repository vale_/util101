use std::ffi::c_uint;

use crate::{Function, Property, Type};

/// A list of properties and additional metadata about a `PropertyClass`.
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct PropertyList {
    _00: [u8; 0x9],
    pub is_singleton: bool,
    _0a: [u8; 0x6],
    pub offset: c_uint,
    _14: [u8; 4],
    pub base_class: *mut PropertyList,
    pub self_type: *mut Type,
    _28: [u8; 0x30],
    pub properties: cxx::Vector<Property>,
    pub functions: cxx::Vector<Function>,
}

assert_eq_size!(PropertyList, [u8; 0x88]);
