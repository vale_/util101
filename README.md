# util101

util101 is a multi-purpose tool for researching the Wizard101 game client.

It loads an interactive REPL where you can enter commands. You can find a list of
supported commands below.

Functionality will be extended as I depend on it.

## Commands

The following is a list of commands that can be used in the util101 REPL.

- `help` - Redirects you to this part.

- `template <id>` - Dumps information about a `CoreTemplate` given its ID as an integer.

  - Example: `template 1`

## Building

To build util101, run `cargo build`.

It is recommended that you do not build with the `--release` flag for now.

We use modest amounts of unsafe code and this program is in a highly experimental state.
The additional integrity checks in debug mode help catch bugs during operation.

## Injecting

It is recommended to use Xenos or another injector that can inject into a process
immediately when it is started.

As util101 depends on very early initialization work done by the game client, this
injection method is vital.

When everything was successful, you should see a REPL prompting you to enter commands.

## Logging

util101 features runtime logging of its doings with various verbosities which
may be useful for troubleshooting and development.

To enable logging, simply set a `UTIL101_LOG` environment variable as explained in detail
[here](https://docs.rs/env_logger/latest/env_logger/#enabling-logging) prior to
injecting the software.

Not setting the environment variable will result in nothing being logged.

## License

util101 is licensed under the terms of the GNU GPLv2.

You can find a copy of the license in the [LICENSE](./LICENSE) file.
