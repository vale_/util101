//! Abstractions for working with C++ code and objects from Rust.

#![deny(unsafe_op_in_unsafe_fn, rustdoc::broken_intra_doc_links)]
#![feature(c_size_t)]

#[macro_use]
extern crate static_assertions;

mod string;
pub use string::*;

mod vector;
pub use vector::*;

mod vtable;
pub use vtable::*;
