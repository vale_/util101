use std::{ffi::c_void, mem, ptr};

#[repr(C)]
struct Bucket<T> {
    obj: *mut T,
    scary: *mut c_void,
}

assert_eq_size!(Bucket<()>, [u8; 0x10]);

/// An ABI-compatible `std::vector` that is borrowed from the C++ side.
///
/// This represents a contiguous storage of elements with pointers to
/// the boundaries. The lifetime duration of said storage is fully managed
/// by C++ to which an immutable view on the Rust side is granted.
///
/// # Safety
///
/// The user must ensure that their handle to a [`Vector`] instance does
/// not outlive the corresponding object on the C++ side and that they don't
/// mutate any of the vector's state.
#[repr(C)]
pub struct Vector<T> {
    head: *mut Bucket<T>,
    tail: *mut Bucket<T>,
    end: *mut Bucket<T>,
}

assert_eq_size!(Vector<()>, [usize; 3]);

impl<T> Vector<T> {
    fn len(&self) -> Option<usize> {
        (self.tail as usize)
            .checked_sub(self.head as usize)
            .map(|size| size / mem::size_of::<Bucket<T>>())
    }

    /// Constructs an iterator over `*mut T` pointers to [`Vector`] elements.
    ///
    /// Element pointers will be yielded in the correct order they were inserted.
    pub fn iter(&mut self) -> VectorIter<T> {
        if let Some(len) = self.len() {
            if !self.head.is_null() {
                // When we have a head pointer that is non-null, we
                // can safely construct the iterator. A size of `0`
                // will result in zero elements being yielded.
                return VectorIter {
                    len,
                    ptr: self.head,
                };
            }
        }

        // We failed to construct a valid iterator, so we return an empty
        // one that will yield no elements before reaching its end.
        VectorIter {
            len: 0,
            ptr: ptr::null_mut(),
        }
    }
}

/// An iterator over the elements inside a [`Vector`].
///
/// The yielded pointers must not be modified from Rust code.
pub struct VectorIter<T> {
    len: usize,
    ptr: *mut Bucket<T>,
}

impl<T> Iterator for VectorIter<T> {
    type Item = *mut T;

    fn next(&mut self) -> Option<Self::Item> {
        // When we have no more items left, we're done.
        if self.len == 0 {
            return None;
        }

        // SAFETY: We trust Vector<T>. Nothing else can construct this type.
        unsafe {
            // Get a pointer to the next vector element.
            let element_ptr = ptr::addr_of_mut!((*self.ptr).obj).read();

            // Update the internal state for the next round.
            self.ptr = self.ptr.wrapping_offset(1);
            self.len -= 1;

            Some(element_ptr)
        }
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}

impl<T> ExactSizeIterator for VectorIter<T> {}
