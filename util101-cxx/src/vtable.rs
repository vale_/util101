use std::{
    ffi::c_void,
    mem::{size_of, transmute_copy},
};

/// Gets a function pointer `F` from `object`'s vtable at the given index.
///
/// # Safety
///
/// The caller is fully responsible for passing a valid pointer to a C++
/// object which does have a vtable, providing an in-bounds index and
/// specifying a valid function pointer type `F`.
#[inline]
pub unsafe fn get_vtable_function<F>(object: *const c_void, idx: usize) -> F {
    debug_assert_eq!(
        size_of::<F>(),
        size_of::<*const c_void>(),
        "`F` is not pointer-sized!"
    );

    let vtable_ptr = object as *const *const *const c_void;
    unsafe {
        // SAFETY: The caller takes full responsible for all inputs.
        let fn_ptr = (*vtable_ptr).wrapping_add(idx).read();
        transmute_copy(&fn_ptr)
    }
}
