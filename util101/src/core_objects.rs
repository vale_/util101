use std::{borrow::Cow, collections::HashMap, ptr};

use anyhow::anyhow;
use cobj::{factory::CoreObjectFactory, CoreObject, CoreTemplate};
use refl::PropertyClass;
use serde::Serialize;

#[derive(Serialize)]
pub struct TemplateDump {
    #[serde(flatten)]
    pub templates: HashMap<String, TemplateInfo>,
}

#[derive(Serialize)]
pub struct TemplateInfo {
    pub core_instance: String,
    pub core_type: u8,
    pub namespace: u8,
    pub template_id: u32,
}

pub fn dump_all_templates() -> anyhow::Result<TemplateDump> {
    let factory = CoreObjectFactory::get();

    let mut dump = TemplateDump {
        templates: HashMap::new(),
    };
    for id in 0..=u32::MAX {
        let template = factory.find_core_template(id);
        if template.is_null() {
            continue;
        }

        let core_instance = unsafe { factory.create_core_instance(template) };

        // SAFETY: `template` is non-null at this point.
        unsafe {
            let name = get_name(template as *mut _)?;
            if dump.templates.contains_key(&name[..]) {
                continue;
            }
            let name = name.to_string();
            let core_type = CoreTemplate::get_core_type(template);
            let namespace = CoreTemplate::get_namespace_id(template);
            let template_id = CoreTemplate::get_template_id(template);
            let core_instance_name = if core_instance.is_null() {
                "???".to_string()
            } else {
                get_name(core_instance as *mut _)?.to_string()
            };

            if !core_instance.is_null() {
                let full_id = template_id as u64 | ((namespace as u64) << 40);
                let actual_full_id = (*core_instance).template_id;

                if full_id != actual_full_id {
                    warn!("ID mismatch - expected {full_id}, got {actual_full_id}");
                }
            }

            if !core_instance.is_null() {
                CoreObject::free(core_instance);
            }

            dump.templates.insert(
                name,
                TemplateInfo {
                    core_instance: core_instance_name,
                    core_type,
                    namespace,
                    template_id,
                },
            );
        }
    }

    Ok(dump)
}

pub fn dump_template(id: u32) -> anyhow::Result<()> {
    let factory = CoreObjectFactory::get();

    let template = factory.find_core_template(id);
    if template.is_null() {
        return Err(anyhow!("Cannot find CoreTemplate with ID {}", id));
    }

    let core_instance = unsafe { factory.create_core_instance(template) };
    if core_instance.is_null() {
        return Err(anyhow!(
            "Failed to create CoreObject instance from template with ID {}",
            id
        ));
    }

    // SAFETY: `template` is non-null at this point.
    unsafe {
        let template_name = get_name(template as *mut _)?;
        let core_type = CoreTemplate::get_core_type(template);
        let core_instance_name = get_name(core_instance as *mut _)?;

        info!("-- Template Name: {}", template_name);
        info!("-- CoreType:      {}", core_type);
        info!("-- Core Instance: {}", core_instance_name);
        info!("     Global ID:   {:08x}", (*core_instance).global_id);
        info!("     Perm ID:     {:08x}", (*core_instance).perm_id);
        info!("     Template ID: {:08x}", (*core_instance).template_id);

        CoreObject::free(core_instance);
    }

    Ok(())
}

unsafe fn get_name<'a>(object: *mut PropertyClass) -> anyhow::Result<Cow<'a, str>> {
    let property_list = PropertyClass::property_list(object);
    if property_list.is_null() {
        return Err(anyhow!("No PropertyList found for given object"));
    }

    let object_type = ptr::addr_of_mut!((*property_list).self_type).read();
    if object_type.is_null() {
        return Err(anyhow!("No Type object found for `object` in PropertyList"));
    }

    Ok((*ptr::addr_of!((*object_type).name))
        .view()
        .to_string_lossy())
}
