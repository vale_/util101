use std::borrow::Cow;

use owo_colors::OwoColorize;
use rustyline::{
    completion::{extract_word, Candidate, Completer},
    highlight::Highlighter,
    hint::Hinter,
    validate::Validator,
};
use rustyline_derive::Helper;

use super::commands::Command;

#[derive(Helper)]
pub struct Helper {
    commands: &'static [&'static str],
}

impl Default for Helper {
    fn default() -> Self {
        Self {
            commands: Command::variants(),
        }
    }
}

pub struct CompletionCandidate {
    display: &'static str,
}

impl Candidate for CompletionCandidate {
    fn display(&self) -> &str {
        self.display
    }

    fn replacement(&self) -> &str {
        self.display
    }
}

impl Completer for Helper {
    type Candidate = CompletionCandidate;

    fn complete(
        &self,
        line: &str,
        pos: usize,
        _ctx: &rustyline::Context<'_>,
    ) -> rustyline::Result<(usize, Vec<Self::Candidate>)> {
        let (idx, word) = extract_word(line, pos, None, &[]);

        let commands = self
            .commands
            .iter()
            .filter(|cmd| cmd.starts_with(word))
            .map(|x| CompletionCandidate { display: x })
            .collect::<Vec<_>>();

        Ok((idx, commands))
    }
}

impl Highlighter for Helper {
    fn highlight_prompt<'b, 's: 'b, 'p: 'b>(
        &'s self,
        prompt: &'p str,
        _default: bool,
    ) -> Cow<'b, str> {
        Cow::Owned(prompt.dimmed().to_string())
    }

    fn highlight_hint<'h>(&self, hint: &'h str) -> Cow<'h, str> {
        Cow::Owned(hint.dimmed().to_string())
    }
}

impl Hinter for Helper {
    type Hint = String;
}

impl Validator for Helper {}
