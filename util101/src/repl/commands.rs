use std::str::FromStr;

use anyhow::anyhow;
use nom::{
    branch::*, bytes::complete::*, character::complete::*, combinator::*, multi::*, sequence::*,
    IResult,
};
use num_traits::{Num, PrimInt, Signed, Unsigned};

macro_rules! build_command_list {
    ($(#[$($m:meta),+])+ pub enum $e:ident { $($(#[$vm:meta])+ $v:ident$(($($ty:path),+))?),+$(,)? }) => {
        $(#[$($m),+])+
        pub enum $e {
            $(
            $(#[$vm])+
            $v$(($($ty),+))?
            ),+
        }

        paste::paste! {
            impl $e {
                pub const fn variants() -> &'static [&'static str] {
                    &[$(stringify!([<$v:lower>])),+]
                }
            }
        }
    };
}

build_command_list! {
    /// Commands that can be executed from the util101 REPL.
    #[derive(Clone, Copy, Debug, PartialEq)]
    pub enum Command {
        /// Prints usage details for the REPL.
        Help,
        /// Dumps the information of a `CoreTemplate` matching the given ID.
        Template(u32),
        /// Dumps the information of all `CoreTemplate`s in the game.
        Templates,
        /// Any unrecognized command.
        Unrecognized,
    }
}

impl FromStr for Command {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match parse_command(s) {
            Ok((_, c)) => Ok(c),
            Err(e) => Err(anyhow!("{}", e)),
        }
    }
}

fn parse_command(input: &str) -> IResult<&str, Command> {
    let (input, command) = ws0(identifier)(input)?;

    let (mut input, command) = match command {
        "help" => (input, Command::Help),
        "template" => {
            let (input, id) = ws0(unsigned)(input)?;
            (input, Command::Template(id))
        }
        "templates" => (input, Command::Templates),
        _ => (input, Command::Unrecognized),
    };

    // When the command is unrecognized, it may have additional arguments
    // at the end instead of the 'expected' EOF for any full command.
    // This can lead to confusing parsing errors instead of the regular
    // handling routine for unexpected commands being called.
    if command != Command::Unrecognized {
        input = eof(input)?.0;
    }

    Ok((input, command))
}

// *whitespace** *parser* *whitespae**
fn ws0<'a, T>(
    parser: impl FnMut(&'a str) -> IResult<&'a str, T>,
) -> impl FnMut(&'a str) -> IResult<&'a str, T> {
    delimited(many0(whitespace), parser, many0(whitespace))
}

// ` ` | 0x09 | 0x0b | 0x0c | 0x20
fn whitespace(input: &str) -> IResult<&str, &str> {
    recognize(one_of(" \t\x0b\x0c\r"))(input)
}

// ( *alpha1* | `_` ) ( *alphanumeric1* | `_` )*
fn identifier(input: &str) -> IResult<&str, &str> {
    recognize(pair(
        alt((alpha1, tag("_"))),
        many0(alt((alphanumeric1, tag("_")))),
    ))(input)
}

#[inline]
fn parse_number<T>(literal: &str, radix: u32) -> Result<T, <T as Num>::FromStrRadixErr>
where
    T: PrimInt,
{
    T::from_str_radix(&str::replace(literal, "_", ""), radix)
}

// *signed_hex* | *signed_binary* | *signed_decimal*
fn signed<T>(input: &str) -> IResult<&str, T>
where
    T: PrimInt + Signed + From<bool>,
{
    alt((signed_hex, signed_binary, signed_decimal))(input)
}

// *unsigned_hex* | *unsigned_binary* | *unsigned_decimal*
fn unsigned<T>(input: &str) -> IResult<&str, T>
where
    T: PrimInt + Unsigned,
{
    alt((unsigned_hex, unsigned_binary, unsigned_decimal))(input)
}

// *sign* *decimal_digits*
fn signed_decimal<T>(input: &str) -> IResult<&str, T>
where
    T: PrimInt + Signed + From<bool>,
{
    map_res(pair(sign, decimal_digits), |(sign, span)| {
        parse_number(span, 10).map(|n: T| {
            let sign: T = From::from(sign);
            (n ^ -sign) + sign
        })
    })(input)
}

// `+`? *decimal_digits*
fn unsigned_decimal<T>(input: &str) -> IResult<&str, T>
where
    T: PrimInt + Unsigned,
{
    map_res(pair(opt(tag("+")), decimal_digits), |(_, span)| {
        parse_number(span, 10)
    })(input)
}

// ( `0` | `1` | `2` | `3` | `4` | `5` | `6` | `7` | `8` | `9` | `_` )+
fn decimal_digits(input: &str) -> IResult<&str, &str> {
    recognize(many1(terminated(one_of("0123456789"), many0(char('_')))))(input)
}

// *sign* *hex_prefix* *hex_digits*
fn signed_hex<T>(input: &str) -> IResult<&str, T>
where
    T: PrimInt + Signed + From<bool>,
{
    map_res(
        pair(sign, preceded(hex_prefix, hex_digits)),
        |(sign, span)| {
            parse_number(span, 16).map(|n: T| {
                let sign: T = From::from(sign);
                (n ^ -sign) + sign
            })
        },
    )(input)
}

// `+`? *hex_prefix* *hex_digits*
fn unsigned_hex<T>(input: &str) -> IResult<&str, T>
where
    T: PrimInt + Unsigned,
{
    map_res(
        pair(opt(tag("+")), preceded(hex_prefix, hex_digits)),
        |(_, span)| parse_number(span, 16),
    )(input)
}

// `0x`
fn hex_prefix(input: &str) -> IResult<&str, &str> {
    complete(tag_no_case("0x"))(input)
}

// ( `0` | `1` | `2` | `3` | `4` | `5` | `6` | `7` | `8` | `9` | `a` | ... )+
fn hex_digits(input: &str) -> IResult<&str, &str> {
    recognize(many1(terminated(
        one_of("0123456789abcdefABCDEF"),
        many0(char('_')),
    )))(input)
}

// *sign* *binary_prefix* *binary_digits*
fn signed_binary<T>(input: &str) -> IResult<&str, T>
where
    T: PrimInt + Signed + From<bool>,
{
    map_res(
        pair(sign, preceded(binary_prefix, binary_digits)),
        |(sign, span)| {
            parse_number(span, 2).map(|n: T| {
                let sign: T = From::from(sign);
                (n ^ -sign) + sign
            })
        },
    )(input)
}

fn unsigned_binary<T>(input: &str) -> IResult<&str, T>
where
    T: PrimInt + Unsigned,
{
    map_res(
        pair(opt(tag("+")), preceded(binary_prefix, binary_digits)),
        |(_, span)| parse_number(span, 2),
    )(input)
}

// `0b`
fn binary_prefix(input: &str) -> IResult<&str, &str> {
    complete(tag_no_case("0b"))(input)
}

// ( `0` | `1` | `_` )+
fn binary_digits(input: &str) -> IResult<&str, &str> {
    recognize(many1(terminated(one_of("01"), many0(char('_')))))(input)
}

// ( `+` | `-` )?
fn sign(input: &str) -> IResult<&str, bool> {
    map(
        opt(alt((value(false, tag("+")), value(true, tag("-"))))),
        |i| i.unwrap_or(false),
    )(input)
}
