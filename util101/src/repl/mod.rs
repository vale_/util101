use std::{fs::File, str::FromStr};

use anyhow::anyhow;
use rustyline::{
    error::ReadlineError, Cmd, CompletionType, Config, EditMode, Editor, KeyCode, KeyEvent,
    Modifiers,
};

use crate::core_objects;

#[allow(dead_code)] // TODO: Remove once a command accepting signed integers exists.
mod commands;
use commands::Command;

mod helper;
use helper::Helper;

const PROMPT: &str = "util101> ";

const HELP_PAGE: &str = concat!(env!("CARGO_PKG_REPOSITORY"), "#commands");

/// Runs the REPL for command processing until an EOF will abort it.
pub fn run() {
    let mut editor = build_editor();
    info!("REPL loaded, type 'help' for more information");

    loop {
        let line = editor.readline(PROMPT);
        match line {
            Ok(line) if !line.is_empty() => {
                if let Err(e) = process_command(&line) {
                    error!("{}", e);
                }
            }
            // Empty line.
            Ok(_) => continue,
            // Ctrl + c will abort the current input.
            Err(ReadlineError::Interrupted) => continue,
            // Ctrl + d will exit the repol.
            Err(ReadlineError::Eof) => return,
            Err(e) => error!("REPL produced an error: {}", e),
        }
    }
}

pub fn process_command(cmd: &str) -> anyhow::Result<()> {
    // Try to parse the next command from the given input.
    let cmd = Command::from_str(cmd)?;
    match cmd {
        Command::Help => info!("{}", HELP_PAGE),

        Command::Template(id) => {
            info!("Dumping Template with ID {}:", id);
            core_objects::dump_template(id)?;
        }

        Command::Templates => {
            info!("Dumping all Templates in the game...");

            let dump = core_objects::dump_all_templates()?;

            let mut file = File::create("core_templates.json")?;
            serde_json::to_writer_pretty(&mut file, &dump)?;

            info!("Done!");
        }

        Command::Unrecognized => return Err(anyhow!("unrecognized command")),
    }

    Ok(())
}

fn build_editor() -> Editor<Helper> {
    let config = Config::builder()
        .history_ignore_space(false)
        .completion_type(CompletionType::List)
        .edit_mode(EditMode::Emacs)
        .max_history_size(1000)
        .build();

    let mut editor = Editor::with_config(config);
    editor.set_helper(Some(Helper::default()));

    editor.bind_sequence(
        KeyEvent(KeyCode::Up, Modifiers::NONE),
        Cmd::LineUpOrPreviousHistory(1),
    );
    editor.bind_sequence(
        KeyEvent(KeyCode::Down, Modifiers::NONE),
        Cmd::LineDownOrNextHistory(1),
    );
    editor.bind_sequence(KeyEvent(KeyCode::Tab, Modifiers::NONE), Cmd::Complete);

    editor
}
