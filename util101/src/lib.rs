#![deny(rustdoc::broken_intra_doc_links)]

#[macro_use]
extern crate log;

use std::{ffi::c_void, ptr};

use anyhow::anyhow;
use common::windows::Module;
use windows::Win32::{
    Foundation::{CloseHandle, BOOL, HINSTANCE},
    System::{
        Console,
        LibraryLoader::DisableThreadLibraryCalls,
        SystemServices::{DLL_PROCESS_ATTACH, DLL_PROCESS_DETACH},
        Threading::{CreateThread, THREAD_CREATE_RUN_IMMEDIATELY},
    },
};

mod core_objects;

mod init;

mod repl;

#[inline(never)]
unsafe extern "system" fn bootstrap_util101(_: *mut c_void) -> u32 {
    fn wrapper() -> anyhow::Result<()> {
        pretty_env_logger::init_custom_env("UTIL101_LOG");

        let module = Module::pe().ok_or_else(|| anyhow!("failed to find module"))?;
        info!("Found {}", module);

        init::global_init(&module)?;

        repl::run();

        Ok(())
    }

    wrapper().is_err() as u32
}

unsafe fn main(module: HINSTANCE, call_reason: u32) -> anyhow::Result<()> {
    match call_reason {
        DLL_PROCESS_ATTACH => {
            DisableThreadLibraryCalls(module).ok()?;
            Console::AllocConsole().ok()?;

            CloseHandle(CreateThread(
                ptr::null(),
                0,
                Some(bootstrap_util101),
                ptr::null(),
                THREAD_CREATE_RUN_IMMEDIATELY,
                ptr::null_mut(),
            ))
            .ok()?;

            Ok(())
        }
        DLL_PROCESS_DETACH => {
            Console::FreeConsole().ok()?;
            Ok(())
        }
        _ => Ok(()),
    }
}

/// Entrypoint to tool101.
#[allow(clippy::missing_safety_doc, non_snake_case)]
#[no_mangle]
pub unsafe extern "stdcall" fn DllMain(
    module: HINSTANCE,
    call_reason: u32,
    _reserved: *const c_void,
) -> BOOL {
    main(module, call_reason).is_ok().into()
}
