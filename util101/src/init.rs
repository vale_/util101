use common::windows::Module;

/// Performs global initialization work for util101.
pub fn global_init(module: &Module) -> anyhow::Result<()> {
    cobj::factory::initialize(module)?;

    Ok(())
}
