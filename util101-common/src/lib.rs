//! Common code shared across util101 crate.

#![deny(unsafe_op_in_unsafe_fn, rustdoc::broken_intra_doc_links)]

pub mod disasm;

pub mod types;

pub mod windows;
