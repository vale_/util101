//! Binary disassembling functionality for host code.

use std::{mem, slice};

use iced_x86::{Decoder, DecoderOptions};

const TARGET_BITNESS: u32 = mem::size_of::<usize>() as u32 * 8;

/// Attempts to disassemble a **near** branch at memory address `addr`.
///
/// On success, this will return the absolute branch target address
/// in memory. [`None`] will be returned if no branch was disassembled.
///
/// # Safety
///
/// - `addr` must be non-null and well-aligned
///
/// - `addr` must point to process-readable memory
///
/// - `addr` must point to **15** consecutively initialized bytes
pub unsafe fn try_disassemble_branch(addr: *const u8) -> Option<usize> {
    // 15 is the maximum length of any valid instruction.
    // Disassembling will always succeed if `addr` points to *any* code.
    let data = unsafe { slice::from_raw_parts(addr, 15) };

    let mut decoder = Decoder::with_ip(
        TARGET_BITNESS,
        data,
        addr as usize as u64,
        DecoderOptions::NONE,
    );

    let next_branch = decoder.decode().near_branch_target();
    // Determine if we actually disassembled a branch instruction here.
    if next_branch != 0 {
        // We can cast with no regrets as we're always disassembling
        // code of a running host process and never static code.
        Some(next_branch as usize)
    } else {
        None
    }
}
