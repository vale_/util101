//! Miscellaneous types commonly used in KingsIsle code.

mod math;
pub use math::*;

mod pointer;
pub use pointer::*;
