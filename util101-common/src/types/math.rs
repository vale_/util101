/// 3D vector for math.
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct Vector3D {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}
