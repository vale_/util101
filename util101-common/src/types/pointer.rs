use std::ffi::c_void;

/// A reference-counted pointer that automatically manages the storage
/// duration of the pointed-to object.
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct SharedPointer<T> {
    pub value: *mut T,
    pub refcount: *mut c_void,
}
