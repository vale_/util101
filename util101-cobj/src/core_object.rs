use std::ffi::{c_short, c_uint, c_ushort, c_void};

use common::types::{SharedPointer, Vector3D};

use crate::CoreTemplate;

// CoreObject::~CoreObject();
type Vtable8 = unsafe extern "fastcall" fn(/* this: */ *mut CoreObject);

/// Representation of a CoreObject, a subclass of PropertyClass.
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct CoreObject {
    vtable: *mut c_void,
    _08: [u8; 0x40],
    pub global_id: u64,
    pub perm_id: u64,
    pub core_template: *mut CoreTemplate,
    pub template_id: u64,
    pub debug_name: cxx::Str,
    pub display_key: cxx::Str,
    pub location: Vector3D,
    pub orientation: Vector3D,
    pub speed_multiplier: c_short,
    pub mobile_id: c_ushort,
    pub scale: f32,
    _c8: [u8; 0x18],
    pub inactive_behaviors: cxx::Vector<SharedPointer<BehaviorInstance>>,
    _f8: [u8; 0x60],
    pub zone_id: c_uint,
}

impl CoreObject {
    /// Frees the allocated object.
    ///
    /// # Safety
    ///
    /// The pointer must be non-null, well-aligned and point to a valid
    /// `CoreObject` allocation.
    pub unsafe fn free(this: *mut CoreObject) {
        unsafe {
            let free: Vtable8 = cxx::get_vtable_function(this as *const c_void, 1);
            (free)(this)
        }
    }
}

assert_eq_size!(CoreObject, [u8; 0x160]);

/// A behavior instance created from a template.
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct BehaviorInstance {
    vtable: *mut c_void,
    _08: [u8; 0x60],
    pub behavior_template_name_id: c_uint,
}

assert_eq_size!(BehaviorInstance, [u8; 0x70]);
