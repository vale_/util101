//! Rust abstractions for KingsIsle's CoreObject system.

#![deny(unsafe_op_in_unsafe_fn, rustdoc::broken_intra_doc_links)]
#![feature(core_ffi_c, once_cell)]

#[macro_use]
extern crate static_assertions;

#[macro_use]
extern crate log;

mod core_object;
pub use core_object::*;

mod core_template;
pub use core_template::*;

pub mod factory;
