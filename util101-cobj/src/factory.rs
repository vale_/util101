//! Implementation of CoreObject logic.

use std::{ffi::c_void, lazy::SyncOnceCell, mem::transmute, sync::mpsc::sync_channel};

use anyhow::anyhow;
use common::{disasm::try_disassemble_branch, windows::Module};
use detour::static_detour;

use crate::{CoreObject, CoreTemplate};

const PRE_LOAD_OBJECT_SIG: &str =
    "48 8b ?? 56 57 41 ?? 48 83 ?? ?? ?? c7 ?? ?? ?? ?? ?? ?? ?? 89 ?? ?? ?? 89 ?? ?? 49 8b ?? 4d";
const LOAD_TEMPLATE_MANIFEST_SIG: &str = "40 ?? 48 83 ?? ?? ?? c7 ?? ?? ?? ?? ?? ?? ?? ?? 89 ?? ?? ?? ?? ?? ?? 48 8b ?? ?? ?? ?? ?? 48 33 ?? ?? 89 ?? ?? ?? 48 8b ?? b9 70";
const FIND_CORE_TEMPLATE_SIG: &str =
    "?? 89 ?? ?? ?? 53 48 83 ?? ?? ?? c7 ?? ?? ?? ?? ?? ?? ?? 48 8b ?? 45 8b";
const CREATE_CORE_INSTANCE_SIG: &str = "?? 89 ?? ?? ?? ?? 89 ?? ?? ?? 57 48 83 ?? ?? 48 ?? ?? 48 8b ?? 48 8b ?? 41 0f ?? ?? 48 8b ?? ?? ?? ?? 0f";

type FnCoreObjectValidator =
    unsafe extern "fastcall" fn(/* this: */ *mut CoreObject) -> *mut CoreObject;
static CORE_OBJECT_VALIDATOR: SyncOnceCell<FnCoreObjectValidator> = SyncOnceCell::new();

type FnFindCoreTemplate = unsafe extern "fastcall" fn(
    /* this: */ *mut c_void,
    /* out: */ *mut c_void,
    /* id: */ u32,
) -> *mut c_void;
static FIND_CORE_TEMPLATE: SyncOnceCell<FnFindCoreTemplate> = SyncOnceCell::new();

type FnCreateCoreInstance = unsafe extern "fastcall" fn(
    /* this: */ *mut c_void,
    /* template: */ *mut CoreTemplate,
    /* unk: */ u8,
) -> *mut CoreObject;
static CREATE_CORE_INSTANCE: SyncOnceCell<FnCreateCoreInstance> = SyncOnceCell::new();

static_detour! {
    static LOAD_TEMPLATE_MANIFEST: unsafe extern "fastcall" fn(/* this: */ *mut c_void);
}

static GLOBAL_INSTANCE: SyncOnceCell<CoreObjectFactory> = SyncOnceCell::new();

/// Representation of the global factory to construct CoreObjects.
#[derive(Debug)]
#[repr(transparent)]
pub struct CoreObjectFactory(*mut c_void);

// SAFETY: We only expose safe access to those functions which can be
// called without any issues in multi-threaded environments.
unsafe impl Sync for CoreObjectFactory {}
unsafe impl Send for CoreObjectFactory {}

impl CoreObjectFactory {
    /// Gets the global instance of the factory.
    ///
    /// # Panics
    ///
    /// When [`initialize`] was not called yet and the global factory
    /// therefore is in an uninitialized state, this will panic.
    #[inline]
    pub fn get() -> &'static CoreObjectFactory {
        GLOBAL_INSTANCE
            .get()
            .expect("global CoreObjectFactory was not yet initialized")
    }

    /// Attempts to find a [`CoreTemplate`] for the given template ID.
    ///
    /// When no template could be found, this method returns a null
    /// pointer.
    pub fn find_core_template(&self, id: u32) -> *mut CoreTemplate {
        let mut out = [0usize; 2];

        // Try to find the core template for this object.
        unsafe {
            (FIND_CORE_TEMPLATE.get().unwrap())(
                self.0,
                out.as_mut_ptr() as *mut u8 as *mut c_void,
                id,
            );
        }

        out[0] as *mut CoreTemplate
    }

    /// Attempts to create a CoreObject instance given its [`CoreTemplate`].
    ///
    /// # Safety
    ///
    /// `template` must be a non-null, well-aligned pointer to a valid
    /// `CoreTemplate` allocation.
    pub unsafe fn create_core_instance(&self, template: *mut CoreTemplate) -> *mut CoreObject {
        let obj = unsafe { (CREATE_CORE_INSTANCE.get().unwrap())(self.0, template, 0) };
        let obj = unsafe { (CORE_OBJECT_VALIDATOR.get().unwrap())(obj) };

        obj
    }
}

/// Initializes the global [`CoreObjectFactory`] for use in Rust code.
///
/// This function will block the executing thread until the initialization
/// is done.
pub fn initialize(module: &Module) -> anyhow::Result<()> {
    info!("Trying to initialize global CoreObjectFactory...");

    let load_template_manifest_fn = module.find_signature(LOAD_TEMPLATE_MANIFEST_SIG)?;
    debug!(
        "load_template_manifest_fn: {:#p}",
        load_template_manifest_fn
    );

    let find_core_template_fn = module.find_signature(FIND_CORE_TEMPLATE_SIG)?;
    debug!("find_core_template_fn: {:#p}", find_core_template_fn);
    let _ = FIND_CORE_TEMPLATE.set(unsafe { transmute(find_core_template_fn) });

    let create_core_instance_fn = module.find_signature(CREATE_CORE_INSTANCE_SIG)?;
    debug!("create_core_instance_fn: {:#p}", create_core_instance_fn);
    let _ = CREATE_CORE_INSTANCE.set(unsafe { transmute(create_core_instance_fn) });

    let core_object_validator_fn = {
        let pre_load_object_fn = module.find_signature(PRE_LOAD_OBJECT_SIG)?;

        // SAFETY: `pre_load_object_fn` is found by sig scan and KI hasn't
        // changed the code in question in ages. Breakage anytime soon is unlikely.
        unsafe {
            try_disassemble_branch(pre_load_object_fn.add(0x384))
                .map(|func| {
                    debug!("Successfully disassembled branch target: {:#x}", func);
                    func as *const u8
                })
                .ok_or_else(|| anyhow!("failed to disassemble CALL instruction"))?
        }
    };
    let _ = CORE_OBJECT_VALIDATOR.set(unsafe { transmute(core_object_validator_fn) });

    let (sender, receiver) = sync_channel(1);
    unsafe {
        LOAD_TEMPLATE_MANIFEST.initialize(
            transmute(load_template_manifest_fn),
            move |factory| {
                // Call the original function to do initialization work.
                LOAD_TEMPLATE_MANIFEST.call(factory);

                // Store the resulting pointer for us to use in our own code.
                // The factory is a global singleton so this is fine.
                let _ = GLOBAL_INSTANCE.set(CoreObjectFactory(factory));

                // Disable the detour, we don't need it anymore.
                let _ = LOAD_TEMPLATE_MANIFEST.disable();

                // Notify the blocking `initialize` function that we're done.
                let _ = sender.send(());
            },
        )?;

        LOAD_TEMPLATE_MANIFEST.enable()?;
    }

    // Block until the detour for `CoreObjectFactory::LoadTemplateManifest` was triggered.
    let _ = receiver.recv();

    info!("Initialized global CoreObjectFactory!");

    Ok(())
}
