use std::ffi::c_void;

// u8 CoreTemplate::GetCoreType();
type Vtable70 = unsafe extern "fastcall" fn(/* this: */ *mut CoreTemplate) -> u8;
// u32 CoreTemplate::GetTemplateID();
type Vtable78 = unsafe extern "fastcall" fn(/* this: */ *mut CoreTemplate) -> u32;
// u8 CoreTemplate::GetNamespaceID();
type Vtable80 = unsafe extern "fastcall" fn(/* this: */ *mut CoreTemplate) -> u8;

/// Representation of a CoreTemplate storing the information to construct
/// a particular CoreObject.
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct CoreTemplate {
    vtable: *mut c_void,
    _08: [u8; 0x40],
    pub behaviors: cxx::Vector<*mut BehaviorTemplate>,
}

assert_eq_size!(CoreTemplate, [u8; 0x60]);

/// A behavior template associated with a [`CoreTemplate`].
///
/// Behavior templates are the foundation for behavior instances
/// assigned to `CoreObject`s on construction.
///
/// This Rust type can be used for FFI with C++ code.
#[repr(C)]
pub struct BehaviorTemplate {
    vtable: *mut c_void,
    _08: [u8; 0x40],
    pub behavior_name: cxx::Str,
}

assert_eq_size!(BehaviorTemplate, [u8; 0x68]);

impl CoreTemplate {
    /// Gets the core type/class ID associated with this core template.
    ///
    /// # Safety
    ///
    /// The pointer must be non-null, well-aligned and point to a valid
    /// `CoreTemplate` allocation.
    pub unsafe fn get_core_type(this: *mut CoreTemplate) -> u8 {
        unsafe {
            let getter: Vtable70 = cxx::get_vtable_function(this as *const c_void, 14);
            (getter)(this)
        }
    }

    /// Gets the template ID associated with this core template.
    ///
    /// # Safety
    ///
    /// The pointer must be non-null, well-aligned and point to a valid
    /// `CoreTemplate` allocation.
    pub unsafe fn get_template_id(this: *mut CoreTemplate) -> u32 {
        unsafe {
            let getter: Vtable78 = cxx::get_vtable_function(this as *const c_void, 15);
            (getter)(this)
        }
    }

    /// Gets the namespace ID associated with this core template.
    ///
    /// # Safety
    ///
    /// The pointer must be non-null, well-aligned and point to a valid
    /// `CoreTemplate` allocation.
    pub unsafe fn get_namespace_id(this: *mut CoreTemplate) -> u8 {
        unsafe {
            let getter: Vtable80 = cxx::get_vtable_function(this as *const c_void, 16);
            (getter)(this)
        }
    }
}
